
# Excel Inserter

This project is a task for a [Mobile Doctors](http://mobiledoctors24-7.com/) interview.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

- A PHP development stack is required for this project to run
- Linux machine.
- Composer.


### Installing

1. Clone the repo ``$ git clone https://gitlab.com/osama-interviews-tasks/excel-inserter.git``
2. `cd` into the project folder and Install the composer packages ``$ cd excel-inserter && composer install``
3. Set you environments variables inside a ``.env`` file in the project root folder. *MAKE SURE YOU SET YOUR **QUEUE_CONNECTION** VARIABLE TO BE **database***
4. Run the database migrations ``$ php artisan migrate``


## Usage

This project provides two commands:
 1. `$ php artisan excel:import ../path/to/excel.xlsx`
     Which uploads and starts a queue to insert the user records from the spreadsheet into the database
2. `$ php artisan excel:export-invalid [-d|--delete]`
    Which exports all users with invalid attributes *(a record with any missing attribute)* to a spreadsheet named `failed.xlsx` under `storage/app/`.
    If the `-d|--delete` flag is passed to the command, these records will be deleted from the database.

## Built With

* [Laravel](https://laravel.com/) - The web framework used
* [Laravel Excel](https://laravel-excel.maatwebsite.nl) - The package used for handling spreadsheets

## Authors

* **Osama Aldemeery**  - [Github](https://github.com/aldemeery) | [Gitlab](https://gitlab.com/aldemeery)
