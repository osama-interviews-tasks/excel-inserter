<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class InvalidUsersExport implements FromCollection, WithHeadingRow
{
    use Exportable;

    private $delete;

    public function __construct(bool $delete)
    {
        $this->delete = $delete;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $users = User::invalid()->get();
        $users->prepend(['First Name', 'Second Name', 'Family Name', 'UID']);
        if ($this->delete) {
            User::invalid()->delete();
        }
        return $users;
    }
}
