<?php

namespace App\Imports;

use App\User;
use Validator;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class UsersImport implements ToModel, WithChunkReading, WithHeadingRow, ShouldQueue
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $validator = Validator::make($row, [
            'first_name' => 'required',
            'second_name' => 'required',
            'family_name' => 'required',
            'uid' => 'required'
        ]);

        $valid = 1;

        if ($validator->fails()) {
            $valid = 0;
        }

        return new User([
            'first_name' => $row['first_name'],
            'second_name' => $row['second_name'],
            'family_name' => $row['family_name'],
            'uid' => $row['uid'],
            'valid' => $valid,
        ]);
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
