<?php

namespace App\Console\Commands;

use App\Exports\InvalidUsersExport;
use Illuminate\Console\Command;

class ExportInvalidUsersSheet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'excel:export-invalid  {--d|delete}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export and delete invalid users from database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new InvalidUsersExport($this->option('delete')))->store('failed.xlsx');
    }
}
