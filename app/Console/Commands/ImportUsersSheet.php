<?php

namespace App\Console\Commands;

use App\Imports\UsersImport;
use App\Jobs\ImportUsersChunk;
use Excel;
use Illuminate\Console\Command;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ImportUsersSheet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'excel:import {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import users from excel sheet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file_path = base_path() . '/' . $this->argument('file');
        $parts = explode(DIRECTORY_SEPARATOR, $file_path);
        $file_name = end($parts);
        $contents = file_get_contents($file_path);
        Storage::put($file_name, $contents);
        (new UsersImport())->queue($file_name);
    }
}
